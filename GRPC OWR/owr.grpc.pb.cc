// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: owr.proto

#include "owr.pb.h"
#include "owr.grpc.pb.h"

#include <functional>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/channel_interface.h>
#include <grpcpp/impl/codegen/client_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/message_allocator.h>
#include <grpcpp/impl/codegen/method_handler.h>
#include <grpcpp/impl/codegen/rpc_service_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_callback_handlers.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/sync_stream.h>
namespace owr {

static const char* Owr_method_names[] = {
  "/owr.Owr/receiveMessage",
};

std::unique_ptr< Owr::Stub> Owr::NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options) {
  (void)options;
  std::unique_ptr< Owr::Stub> stub(new Owr::Stub(channel));
  return stub;
}

Owr::Stub::Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel)
  : channel_(channel), rpcmethod_receiveMessage_(Owr_method_names[0], ::grpc::internal::RpcMethod::NORMAL_RPC, channel)
  {}

::grpc::Status Owr::Stub::receiveMessage(::grpc::ClientContext* context, const ::owr::owrMessage& request, ::owr::owrReply* response) {
  return ::grpc::internal::BlockingUnaryCall(channel_.get(), rpcmethod_receiveMessage_, context, request, response);
}

void Owr::Stub::experimental_async::receiveMessage(::grpc::ClientContext* context, const ::owr::owrMessage* request, ::owr::owrReply* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_receiveMessage_, context, request, response, std::move(f));
}

void Owr::Stub::experimental_async::receiveMessage(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::owr::owrReply* response, std::function<void(::grpc::Status)> f) {
  ::grpc_impl::internal::CallbackUnaryCall(stub_->channel_.get(), stub_->rpcmethod_receiveMessage_, context, request, response, std::move(f));
}

void Owr::Stub::experimental_async::receiveMessage(::grpc::ClientContext* context, const ::owr::owrMessage* request, ::owr::owrReply* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_receiveMessage_, context, request, response, reactor);
}

void Owr::Stub::experimental_async::receiveMessage(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::owr::owrReply* response, ::grpc::experimental::ClientUnaryReactor* reactor) {
  ::grpc_impl::internal::ClientCallbackUnaryFactory::Create(stub_->channel_.get(), stub_->rpcmethod_receiveMessage_, context, request, response, reactor);
}

::grpc::ClientAsyncResponseReader< ::owr::owrReply>* Owr::Stub::AsyncreceiveMessageRaw(::grpc::ClientContext* context, const ::owr::owrMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::owr::owrReply>::Create(channel_.get(), cq, rpcmethod_receiveMessage_, context, request, true);
}

::grpc::ClientAsyncResponseReader< ::owr::owrReply>* Owr::Stub::PrepareAsyncreceiveMessageRaw(::grpc::ClientContext* context, const ::owr::owrMessage& request, ::grpc::CompletionQueue* cq) {
  return ::grpc_impl::internal::ClientAsyncResponseReaderFactory< ::owr::owrReply>::Create(channel_.get(), cq, rpcmethod_receiveMessage_, context, request, false);
}

Owr::Service::Service() {
  AddMethod(new ::grpc::internal::RpcServiceMethod(
      Owr_method_names[0],
      ::grpc::internal::RpcMethod::NORMAL_RPC,
      new ::grpc::internal::RpcMethodHandler< Owr::Service, ::owr::owrMessage, ::owr::owrReply>(
          [](Owr::Service* service,
             ::grpc_impl::ServerContext* ctx,
             const ::owr::owrMessage* req,
             ::owr::owrReply* resp) {
               return service->receiveMessage(ctx, req, resp);
             }, this)));
}

Owr::Service::~Service() {
}

::grpc::Status Owr::Service::receiveMessage(::grpc::ServerContext* context, const ::owr::owrMessage* request, ::owr::owrReply* response) {
  (void) context;
  (void) request;
  (void) response;
  return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
}


}  // namespace owr

