#include <iostream>
#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <grpcpp/grpcpp.h>
#include "owr.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;		//last homework, it was pain - started to use these
using grpc::ClientContext;
using grpc::Channel;

using owr::Owr;
using owr::owrMessage;
using owr::owrReply;

using namespace std;	//without this I have a to_string error...too lazy to care.

class CNode : public Owr::Service
{
public:
  CNode() = delete;
  CNode(int, std::string, std::string, std::string, std::string);
  void sendMessageBothWays(int, std::string);

private:
  int m_NodeID;
  
  std::string m_NextNodeAddress;
  std::string m_PrevNodeAddress;
  std::unique_ptr<Owr::Stub> m_NextStub;
  std::unique_ptr<Owr::Stub> m_PrevStub;
  							//v nazvech je bordel - fakt, ze jsem spatne pochopil skeletona a stuba
  							//jsem se dozvedel az kdyz mi to nesedelo a znovu jsem si precetl cviceni
  std::string m_NextSkeletonAddress;
  std::string m_PrevSkeletonAddress;
  std::unique_ptr<Server> m_NextSkeleton;
  std::unique_ptr<Server> m_PrevSkeleton;
  
  std::thread* m_NextSkeletonThread;
  std::thread* m_PrevSkeletonThread;
  std::mutex m_NextLock, m_PrevLock;
  int m_ThreadsRunning;


  void constructSkeleton(bool);
  void sendMessage(int, int, std::string, bool);  
  Status receiveMessage(ServerContext*, const owrMessage*, owrReply*) override;
};

int g_NodesReady = 0;

CNode::CNode(int nodeID, std::string nextNodeAddress, std::string prevNodeAddress, std::string nextSkeletonAddress, std::string prevSkeletonAddress)
  :m_NodeID(nodeID), m_NextNodeAddress(nextNodeAddress), m_PrevNodeAddress(prevNodeAddress), m_NextStub(nullptr), m_PrevStub(nullptr), m_NextSkeletonAddress(nextSkeletonAddress), m_PrevSkeletonAddress(prevSkeletonAddress), m_NextSkeletonThread(nullptr), m_PrevSkeletonThread(nullptr), m_ThreadsRunning(0) 
{ 
  m_NextSkeletonThread = new std::thread(&CNode::constructSkeleton, this, true);
  m_PrevSkeletonThread = new std::thread(&CNode::constructSkeleton, this, false);
  while (2 != m_ThreadsRunning) { }
  
  m_NextStub = Owr::NewStub(grpc::CreateChannel(std::string(m_NextNodeAddress), grpc::InsecureChannelCredentials()));
  m_PrevStub = Owr::NewStub(grpc::CreateChannel(std::string(m_PrevNodeAddress), grpc::InsecureChannelCredentials()));
  
  g_NodesReady++;
}

void CNode::constructSkeleton(bool nextSkeleton)
{
  ServerBuilder builder;
  builder.AddListeningPort(std::string(nextSkeleton ? m_NextSkeletonAddress : m_PrevSkeletonAddress), grpc::InsecureServerCredentials());
  builder.RegisterService(this);
  if(nextSkeleton){
    m_NextSkeleton = builder.BuildAndStart();
    m_ThreadsRunning++;
    m_NextSkeleton->Wait();  				
  }
  else{
    m_PrevSkeleton = builder.BuildAndStart();
    m_ThreadsRunning++;
    m_PrevSkeleton->Wait();
  }
  //m_ThreadsRunning++			??Why cant this be here?? Solve: nops instead of incrementing counter there?					
}

void CNode::sendMessageBothWays(int receiver, std::string content)
{
  m_NextLock.lock();
  sendMessage(m_NodeID, receiver, content, true);
  m_NextLock.unlock();
    
  m_PrevLock.lock();
  sendMessage(m_NodeID, receiver, content, false);
  m_PrevLock.unlock();
}

void CNode::sendMessage(int sender, int receiver, std::string content, bool forward)	
{
  ClientContext context;

  owrMessage message;
  message.set_sender(sender);
  message.set_receiver(receiver);
  message.set_content(content);
  message.set_forward(forward ? 1 : 0);

  owrReply reply;
  Status status = forward ? m_NextStub->receiveMessage(&context, message, &reply) : m_PrevStub->receiveMessage(&context, message, &reply);
  
  if (! status.ok()) 
      std::cout << "Error #" << status.error_code() << ": " << status.error_message() << std::endl;
}

Status CNode::receiveMessage(ServerContext* context, const owrMessage* request, owrReply* reply)
{
  if(request->sender() == m_NodeID){
    std::cout << "Infinite loop" << std::endl;
    reply->set_code(1);
    return Status::OK;
  }

  if (request->receiver() == m_NodeID) {
    std::cout << m_NodeID << ": DELIVERY SUCCESSFULL. Sender: " << request->sender() << ", content: " << request->content() << std::endl;
    reply->set_code(0);
    return Status::OK;
  } 
      
  if(request->forward()){
    m_NextLock.lock();
    sendMessage(request->sender(), request->receiver(),  request->content(), true);
    m_NextLock.unlock();
  }
  else{
    m_PrevLock.lock();
    sendMessage(request->sender(), request->receiver(), request->content(), false);
    m_PrevLock.unlock();
  }
  return Status::OK;		
}

#define NUM_OF_NODES 6
#define PORT 66666
#define IP_ADDRESS "127.0.0.1"

#define MESSAGE "TEST"

int main(int argc, char* argv[])
{
  if(argc < 3){
    std::cout << "Usage: ./owr [Sending node] [Recieving node]" << std::endl;
    return 1;
  }
    
  std::vector<std::shared_ptr<CNode> > ring;
  
  for (int i=0; i < NUM_OF_NODES; i++) {
        
    std::string nextSkeletonAddress(std::string(IP_ADDRESS) + std::string(":") + to_string(PORT + (2 * i)));
    std::string prevSkeletonAddress(std::string(IP_ADDRESS) + std::string(":") + to_string(PORT + (2 * i) + 1));

    std::string nextNodeAddress = (PORT + 2 * i + 1 + 1 > PORT + (NUM_OF_NODES-1) * 2)
    				? std::string(IP_ADDRESS) + std::string(":") + to_string(PORT)
    				: std::string(IP_ADDRESS) + std::string(":") + to_string(PORT + (2 * i) + 1 + 1);

        
    std::string prevNodeAddress = (PORT + 2 * i - 1 > PORT)
    				? std::string(IP_ADDRESS) + std::string(":") + to_string(PORT+2 * i - 1)
    				: std::string(IP_ADDRESS) + std::string(":") + to_string(PORT + NUM_OF_NODES * 2 - 1);
        
    ring.emplace_back(std::make_shared<CNode>(i, nextNodeAddress, prevNodeAddress, nextSkeletonAddress, prevSkeletonAddress));
  }
  
  while(NUM_OF_NODES != g_NodesReady) { }

  ring[atoi(argv[1])]->sendMessageBothWays(atoi(argv[2]), std::string(MESSAGE));
  
  return 0;
}
