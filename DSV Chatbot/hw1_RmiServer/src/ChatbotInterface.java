import java.rmi.*;

public interface ChatbotInterface extends Remote {
	
	String answerMessage(String message) throws RemoteException;
}