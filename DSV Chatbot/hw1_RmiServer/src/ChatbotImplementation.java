import java.rmi.RemoteException;

public class ChatbotImplementation implements ChatbotInterface {
	
	public String[] possibleMessages = {"Ahoj", "Jak je?", "Jaky je smysl zivota?", "Pekny den"};
	public String[] possibleAnswers = {"Cau", "Pekne naprd, ale dobre to zvladam", "42", "Tobe take"};
	public String defaultAnswer = "Ja nerozumnela vas rec";
	
	public String answerMessage(String message) {
		String input = DecipherLine(message);
		for(int i = 0; i < possibleMessages.length; ++i) {
			if(input.equals(possibleMessages[i])) return CipherLine(possibleAnswers[i]);
		}
		
		return CipherLine(defaultAnswer);
	}
	
	public static String CipherLine(String line) {
		StringBuilder ret = new StringBuilder();		//I hate java
		for(int i = 0; i < line.length(); i++) {					
			ret.append((char)(line.charAt(i) + 3));				//Am I really the only one who sees a problem?
		}
		return ret.toString();
	}
	
	public static String DecipherLine(String line) {
		StringBuilder ret = new StringBuilder();		//I hate java
		for(int i = 0; i < line.length(); i++) {					
			ret.append((char)(line.charAt(i) - 3));				//Am I really the only one who sees a problem?
		}
		return ret.toString();
	}
}