import java.rmi.registry.*;
import java.rmi.server.*;

public class Chatbot {
	public static void main(String args[]) {
		
		System.setProperty("java.rmi.server.hostname", "0.0.0.0");
		
		Registry reg = null;
		try {
			//reg = LocateRegistry.getRegistry("0.0.0.0", 10901);
			reg = LocateRegistry.createRegistry(10901);
		}
		catch (Exception e) {
			System.out.println("Error in server: " + e.toString());
		}
		
		ChatbotImplementation bot = new ChatbotImplementation();
		try {
			ChatbotInterface stub = (ChatbotInterface) UnicastRemoteObject.exportObject(bot, 0);
			reg.rebind("ChatbotInterface", stub);
		}
		catch (Exception e) {
			System.out.println("Error in server2: " + e.toString());
		}
	}
}