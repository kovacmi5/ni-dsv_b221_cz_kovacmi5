import java.rmi.registry.*;
import java.util.Scanner;

public class Chatbot {
	
	public static void main(String args[]) {
		System.out.println("Claptrap activated! Type \"exit\" to exit the application." );
		try {
			Registry reg = LocateRegistry.getRegistry("127.0.0.1", 10901);
			
			ChatbotInterface stub = (ChatbotInterface) reg.lookup("ChatbotInterface");
			
			Scanner inputScanner = new Scanner(System.in);
			while(true) {
				System.out.println("Client: ");
				String userInput = inputScanner.nextLine();
				if(userInput.equals("exit")) break;
				String answer = stub.answerMessage(CipherLine(userInput));
				System.out.println("Server: " + DecipherLine(answer));
			}
			inputScanner.close();
		}
		
		catch (Exception e) {System.out.println("Error in Chatbot.java: " + e.toString());}
	}
	
	public static String CipherLine(String line) {
		StringBuilder ret = new StringBuilder();		//I hate java
		for(int i = 0; i < line.length(); i++) {					
			ret.append((char)(line.charAt(i) + 3));		//Am I really the only one who sees a problem?
		}
		return ret.toString();
	}
	
	public static String DecipherLine(String line) {
		StringBuilder ret = new StringBuilder();		//I hate java
		for(int i = 0; i < line.length(); i++) {					
			ret.append((char)(line.charAt(i) - 3));		//Am I really the only one who sees a problem?
		}
		return ret.toString();
	}	
}