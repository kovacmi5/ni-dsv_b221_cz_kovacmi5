//#include <iostream>
#include <string>
#include <map>
//#include <algorithm>
//#include <memory>
//#include <chrono>
//#include <cmath>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>

#include "histogram.grpc.pb.h"

#define SERVER_ADDRESS "0.0.0.0:50051"

class histogramServiceImplementation final : public histogram::Histogram::Service {
public:
	grpc::Status createHistogram(grpc::ServerContext* context, grpc::ServerReaderWriter<histogram::histogramMessage, histogram::histogramMessage>* stream) override;

private:
	std::map<std::string, int> sol = {};
};

grpc::Status histogramServiceImplementation::createHistogram(grpc::ServerContext* context, grpc::ServerReaderWriter<histogram::histogramMessage, histogram::histogramMessage>* stream)
{
	histogram::histogramMessage msg;
	
	while(stream->Read(&msg)){
		if(sol.find(msg.message()) != sol.end()) 
			(sol.find((msg.message()))->second++);
		else
			sol.insert(std::make_pair(msg.message(), 1));	//why {} not work?
	}
	
	
	std::string s("");
	histogram::histogramMessage reply;
	for(std::map<std::string,int>::iterator it = sol.begin(); it != sol.end(); it++)
		s+= it->first + ": " + std::to_string(it->second) + "\n";
	
	reply.set_message(s);
	stream->Write(reply);
	return grpc::Status::OK;
}

int main()
{
	histogramServiceImplementation serviceImplementation;
	grpc::ServerBuilder serverBuilder;
	serverBuilder.AddListeningPort(SERVER_ADDRESS, grpc::InsecureServerCredentials());
	serverBuilder.RegisterService(&serviceImplementation);
	std::unique_ptr<grpc::Server> server(serverBuilder.BuildAndStart());	//a tohle je presne ta chvile, kdy zjistim, ze nevim, kde je leve a kde je prava
	server->Wait();
}
