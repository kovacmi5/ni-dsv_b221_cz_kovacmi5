#include <grpcpp/grpcpp.h>
#include <iostream>
#include <fstream>
#include <string>

#include "histogram.grpc.pb.h"

#define SERVER_ADDRESS "localhost:50051"
#define WORD_FILE_PATH "words.txt"

int main()
{
	std::ifstream words(WORD_FILE_PATH);
	if(!words.good()) {
		std::cout << "File not good" << std::endl; 
		return 1;
	}
		
	std::unique_ptr<histogram::Histogram::Stub> myStub = histogram::Histogram::NewStub(
	(grpc::CreateChannel(std::string(SERVER_ADDRESS), grpc::InsecureChannelCredentials())));	//A JE TO!
	
	
	grpc::ClientContext context;
	std::shared_ptr<grpc::ClientReaderWriter<histogram::histogramMessage, histogram::histogramMessage>> stream (myStub->createHistogram(&context));	//bruh
	
	std::string word("");
	while(!words.eof()){
		words >> word;
		if(words.eof()) break;			//nerikejte tohle ladovi prosim
		histogram::histogramMessage msg;	
		msg.set_message(word);
		stream->Write(msg);
	}
	stream->WritesDone();
	
	histogram::histogramMessage serverMsg;
	while (stream->Read(&serverMsg)) 
		std::cout << "Histogram:\n" << serverMsg.message() << std::endl;
	
	grpc::Status status = stream->Finish();
	if(! status.ok()) {
		std::cout << "Status not ok" << std::endl; 
		return 1;
	}
	
	return 0;	
}
