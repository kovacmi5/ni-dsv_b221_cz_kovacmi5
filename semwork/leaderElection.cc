#include <iostream>
#include <sstream>
#include <vector>
#include <queue>
#include <string>
#include <thread>
#include <mutex>
#include <string_view>
#include <grpcpp/grpcpp.h>
#include <utility>
#include <set>
#include "leaderElection.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;		
using grpc::ClientContext;
using grpc::Channel;

using leaderElection::LeaderElection;
using leaderElection::message;
using leaderElection::reply;

using namespace std;

class CNode : public LeaderElection::Service
{
public:
  CNode() = delete;
  CNode(int, std::string, std::string, std::string, std::string);
  void sendMessageBothWays(int);

  int m_Status;
  int m_NodeID;
  int m_MiniStatus;
  std::set<int> m_NodesWhoVisited;
private:
  std::string m_NextNodeAddress;
  std::string m_PrevNodeAddress;
  std::unique_ptr<LeaderElection::Stub> m_NextStub;
  std::unique_ptr<LeaderElection::Stub> m_PrevStub;

  std::string m_NextSkeletonAddress;
  std::string m_PrevSkeletonAddress;
  std::unique_ptr<Server> m_NextSkeleton;
  std::unique_ptr<Server> m_PrevSkeleton;
  
  std::thread* m_NextSkeletonThread;
  std::thread* m_PrevSkeletonThread;
  std::mutex m_NextLock, m_PrevLock;
  int m_ThreadsRunning;


  void constructSkeleton(bool);
  std::pair<int,int> sendMessage(int, int, bool);  
  Status receiveMessage(ServerContext*, const message*, reply*) override;
};

int gNodesReady = 0;

#define STATUS_DEFEATED 0
#define STATUS_CANDIDATE 1
#define STATUS_LEADER 2

CNode::CNode(int nodeID, std::string nextNodeAddress, std::string prevNodeAddress, std::string nextSkeletonAddress, std::string prevSkeletonAddress)
  :m_NodeID(nodeID), m_Status(STATUS_CANDIDATE), m_MiniStatus(STATUS_CANDIDATE), m_NextNodeAddress(nextNodeAddress), m_PrevNodeAddress(prevNodeAddress), m_NextStub(nullptr), m_PrevStub(nullptr), m_NextSkeletonAddress(nextSkeletonAddress), m_PrevSkeletonAddress(prevSkeletonAddress), m_NextSkeletonThread(nullptr), m_PrevSkeletonThread(nullptr), m_ThreadsRunning(0) 
{ 
  m_NextSkeletonThread = new std::thread(&CNode::constructSkeleton, this, true);
  m_PrevSkeletonThread = new std::thread(&CNode::constructSkeleton, this, false);
  while (2 != m_ThreadsRunning) { }
  
  m_NextStub = LeaderElection::NewStub(grpc::CreateChannel(std::string(m_NextNodeAddress), grpc::InsecureChannelCredentials()));
  m_PrevStub = LeaderElection::NewStub(grpc::CreateChannel(std::string(m_PrevNodeAddress), grpc::InsecureChannelCredentials()));
  
  gNodesReady++;
}

void CNode::constructSkeleton(bool nextSkeleton)
{
  ServerBuilder builder;
  builder.AddListeningPort(std::string(nextSkeleton ? m_NextSkeletonAddress : m_PrevSkeletonAddress), grpc::InsecureServerCredentials());
  builder.RegisterService(this);
  if(nextSkeleton){
    m_NextSkeleton = builder.BuildAndStart();
    m_ThreadsRunning++;
    m_NextSkeleton->Wait();  				
  }
  else{
    m_PrevSkeleton = builder.BuildAndStart();
    m_ThreadsRunning++;
    m_PrevSkeleton->Wait();
  }					
}

#define YES 1
#define NO 0


void CNode::sendMessageBothWays(int distanceOfMessage)
{
  if(STATUS_DEFEATED == m_Status)
    return;

  m_NextLock.lock();
  std::pair<int, int> ret1 = sendMessage(m_NodeID, distanceOfMessage, true);
  m_NextLock.unlock();
    
  m_PrevLock.lock();
  std::pair<int, int> ret2 = sendMessage(m_NodeID, distanceOfMessage, false);
  m_PrevLock.unlock();
  
  if(NO == ret1.first || NO == ret2.first)
    m_MiniStatus = STATUS_DEFEATED;
    
  if(YES == ret1.second || YES == ret2.second)
    m_MiniStatus = STATUS_LEADER;
    
  m_NodesWhoVisited.clear();
}

std::pair<int, int> CNode::sendMessage(int NodeID, int distanceOfMessage, bool forward)	
{
  ClientContext context;

  message msg;
  msg.set_senderid(NodeID);
  msg.set_timestoresent(distanceOfMessage);
  msg.set_forward(forward ? 1 : 0);

  reply rpl;
  Status status = forward ? m_NextStub->receiveMessage(&context, msg, &rpl) : m_PrevStub->receiveMessage(&context, msg, &rpl);
  
  if (! status.ok()) 
      std::cout << "Error #" << status.error_code() << ": " << status.error_message() << std::endl;
      
  return std::pair<int, int>(rpl.code(), rpl.overlap());
}

Status CNode::receiveMessage(ServerContext* context, const message* request, reply* rpl)
{
  //std::cout << "Node " << m_NodeID << " recieved message from Node " << request->senderid() << std::endl;
  
  int foreignID = request->senderid();
  if(m_NodesWhoVisited.find(foreignID) == m_NodesWhoVisited.end())
    m_NodesWhoVisited.insert(foreignID);
  else
    rpl->set_overlap(YES);
  
  if(foreignID > m_NodeID) {
    rpl->set_code(NO);
    //std::cout << "The answer is: NO" << std::endl;
    return Status::OK;
  }
  else {
    m_MiniStatus = STATUS_DEFEATED;
    std::pair<int, int> ret;
    if(0 < request->timestoresent()){
      if(request->forward()){
        m_NextLock.lock();
        ret = sendMessage(request->senderid(), request->timestoresent() - 1, true);
        m_NextLock.unlock();
        rpl->set_code(ret.first);
        rpl->set_overlap(ret.second);
      }
      else{
        m_PrevLock.lock();
        ret = sendMessage(request->senderid(), request->timestoresent() - 1, false);
        m_PrevLock.unlock();
        rpl->set_code(ret.first);
        rpl->set_overlap(ret.second);
      }
    }
    else{
      //std::cout << "The answer is: YES" << std::endl;
      rpl->set_code(YES);
    }
  }
  return Status::OK;
}

unsigned int gNumberOfNodes = 0;
#define PORT 60000
#define IP_ADDRESS "127.0.0.1"

#define MESSAGE "TEST"

int main(int argc, char* argv[])
{
  if(argc != 2){
    std::cout << "Usage: ./leaderElection [value_1,value_2,value_3...]" << std::endl << "Note: NO SPACES" << std::endl;
    std::cout << "./leaderElection 1,11,13,3,9,16,2,14,12,5,15,10,4,8,6,7 are the values from lecture" << std::endl;
    return 1;
  }
  std::stringstream sss(argv[1]);
  std::vector<int> task;

  for(int i; sss >> i;){
    task.emplace_back(i);
    if(sss.peek() == ',') sss.ignore();
    gNumberOfNodes++;
  } 
    
  std::vector<std::shared_ptr<CNode> > ring;
  for (int i=0; i < task.size(); i++) {
    std::string nextSkeletonAddress(std::string(IP_ADDRESS) + std::string(":") + std::to_string(PORT + task[i]));
    std::string prevSkeletonAddress(std::string(IP_ADDRESS) + std::string(":") + std::to_string(PORT + task[i] + 100));
    std::string nextNodeAddress(std::string(IP_ADDRESS) + std::string(":") + std::to_string(PORT + task[(i + 1) % task.size()] + 100));
    std::string prevNodeAddress(std::string(IP_ADDRESS) + std::string(":") + std::to_string(PORT + task[(i + task.size() - 1) % task.size()]));
        
    ring.emplace_back(std::make_shared<CNode>(task[i], nextNodeAddress, prevNodeAddress, nextSkeletonAddress, prevSkeletonAddress));
  }
  
  while(gNumberOfNodes != gNodesReady) { }

  int leaderID = -1;
  int rounds = 0;
  std::vector<std::thread> threadVector;

  for(int k = 1;;k *= 2){
    for(int i = 0; i < ring.size(); ++i){
      threadVector.emplace_back(&CNode::sendMessageBothWays, &(*ring[i]) , k - 1);
    }
    for(int i = 0; i < ring.size(); ++i){
      threadVector[i].join();
    }
    threadVector.clear();
    rounds++;
    
    for(int i = 0; i < ring.size(); ++i){		//for purpose of setting nodes to defeated AFTER each phase, not during...
      ring[i]->m_Status = ring[i]->m_MiniStatus;
    }    
    //std::cout << "Round " << rounds << ": " << std::endl;
    for(int i = 0; i < ring.size(); ++i){
      if(STATUS_LEADER == ring[i]->m_Status){
      	leaderID = ring[i]->m_NodeID;
      }
      //if(STATUS_DEFEATED == ring[i]->m_Status)
      //	std::cout << "Node " << ring[i]->m_NodeID << " eliminated" << std::endl;
    }
    if(-1 != leaderID){
      break;
    }
    //if(5 == rounds)
    //  break;		//testing purposes, please leave commented 
  }

  std::cout << "Leader: " << leaderID << std::endl << "Rounds: " << rounds << std::endl;

  return 0;
}
